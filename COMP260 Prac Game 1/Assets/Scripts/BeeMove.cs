﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	public float speed = 3.0f;
	public float turnSpeed = 180.0f;
	public Transform target;
	public Transform target2;
	public Vector2 heading = Vector3.right;

	void Update(){
		Vector2 direction1 = target.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;
		float distance1 = Vector2.Distance (target.transform.position, transform.position);
		float distance2 = Vector2.Distance (target2.transform.position, transform.position);

		Debug.Log (distance1);

		//if (target2.position < target.position) {
	//		Debug.Log ("direction1");
	//	}
		float angle = turnSpeed * Time.deltaTime;
		if (distance2 < distance1) {

			if (direction2.IsOnLeft (heading)) {
				heading = heading.Rotate (angle);
			} else {
				heading = heading.Rotate (-angle);
			}
			transform.Translate (heading * speed * Time.deltaTime);
		} else {

			if (direction1.IsOnLeft (heading)) {
				heading = heading.Rotate (angle);
			} else {
				heading = heading.Rotate (-angle);
			}
			transform.Translate (heading * speed * Time.deltaTime);
		}
	}
	void OnDrawGizmos(){
		float distance1 = Vector2.Distance (target.transform.position, transform.position);
		float distance2 = Vector2.Distance (target2.transform.position, transform.position);
		if (distance2 > distance1) {
			Gizmos.color = Color.red;
			Gizmos.DrawRay (transform.position, heading);

			Gizmos.color = Color.yellow;
			Vector2 direction = target.position - transform.position;
			Gizmos.DrawRay (transform.position, direction);
		} else {
			Gizmos.color = Color.red;
			Gizmos.DrawRay (transform.position, heading);

			Gizmos.color = Color.yellow;
			Vector2 direction2 = target2.position - transform.position;
			Gizmos.DrawRay (transform.position, direction2);
		}

	}

}
